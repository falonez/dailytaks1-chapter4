// Memanggil Module dotenv
require('dotenv').config();
// Memanggil Module http
const http = require('http');
// Memanggil Module fs
const fs = require('fs');
// Menentukan Port
const PORT = process.env.PORT;
// Memanggil Module path
const path = require("path");
// Mengambil Function di filterdata
const {sortData1, sortData2, sortData3, sortData4, sortData5} = require("./filterdata.js");
 // Mengambil data JSON dari file data
const dataJSON = fs.readFileSync(path.normalize('data.js'));
// Mengubah data ke data JSON
const dataArray = JSON.parse(dataJSON)


// Memmbuat Function untuk memanggil file html
function getHTML(htmlFileName) {
  // menemtukan path file html
  // public merupakan folder
  const htmlFilePath = path.join("public", htmlFileName);
  // Membaca file html / menjalankan file html
  return fs.readFileSync(htmlFilePath, "utf-8");
}

function onRequest(req, res) {
  // Membuat switch case untuk url
  switch(req.url){
   // Direct ke halaman home
    case"/":
       console.log(req.url);
      res.writeHead(200);
      res.end(getHTML("home.html"));
    return;
    // Direct ke halaman about
    case"/about":
       console.log(req.url);
      res.writeHead(200);
      res.end(getHTML("about.html"));
    return;
    // Menampilkan data JSON
    case"/datajson":
       console.log(req.url);
      res.writeHead(200);
      res.end(dataJSON);
    return;
    // Menampilkan data 1
    case"/data1":
       console.log(req.url);
      res.writeHead(200);
      res.end(sortData1(dataArray));
    return;
    // Menampilkan data 2
    case"/data2":
       console.log(req.url);
      res.writeHead(200);
      res.end(sortData2(dataArray));
    return;
    // Menampilkan data 3
    case"/data3":
       console.log(req.url);
      res.writeHead(200);
      res.end(sortData3(dataArray));
    return;
    // Menampilkan data 4
    case"/data4":
       console.log(req.url);
      res.writeHead(200);
      res.end(sortData4(dataArray));
    return;
    // Menampilkan data 5
    case"/data5":
       console.log(req.url);
      res.writeHead(200);
      res.end(sortData5(dataArray));
    return;
    // Direct halaman not_Found
    default:
       console.log(req.url);
      res.writeHead(200);
      res.end(getHTML("not_found.html"));
    return;
  }
 
}

const server = http.createServer(onRequest);

// Jalankan server
server.listen(8000);

console.log("server running on http://localhost:8000");
